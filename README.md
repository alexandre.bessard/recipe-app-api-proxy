# Recipe App API Proxy

NGINX proxy app for our recipe app AMI

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`) 
* `APP_PORT` - POrt of the app to forward requests to (default: `9000`)
